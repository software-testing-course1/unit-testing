import {findOptimalHurwitz} from "../src/app";

describe('Test function "findOptimalHurwitz"', () => {
    it('When risk matrix is empty', () => {
        const riskMatrix = [
            [],
            [],
            []
        ];
        const hurwiczCoeff = 1;
        expect(findOptimalHurwitz(riskMatrix, hurwiczCoeff)).toBe(1);
    });

    it('When risk matrix is 3x4 size', () => {
        const riskMatrix = [
            [1, 12, 4, 7],
            [6, 4, 9, 3],
            [9, 3, 1, 2],
        ];
        const hurwiczCoeff = 1;
        expect(findOptimalHurwitz(riskMatrix, hurwiczCoeff)).toBe(2);
    });
});
