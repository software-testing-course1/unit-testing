import {sum} from '../src/app';

describe('Function "sum"', () => {
    it('When array is empty', () => {
        expect(sum([])).toBe(0);
    });

    it('When array with only positive elements', () => {
        expect(sum([1, 2, 3])).toBe(6);
    });

    it('When array with negative elements', () => {
        expect(sum([1, -2, 3])).toBe(2);
    });

    it('When array with only negative elements', () => {
        expect(sum([-1, -2, -3])).toBe(-6);
    });
})
