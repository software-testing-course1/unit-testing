import {findOptimalWild} from "../src/app";

describe('Test function "findOptimalWild"', () => {
   it('When risk matrix is empty', () => {
       const riskMatrix = [
           [],
           [],
           []
       ]
       expect(findOptimalWild(riskMatrix)).toBe(1);
   });

    it('When risk matrix is 3x4 size', () => {
        const riskMatrix = [
            [4, 3, 2, 3],
            [4, 2, 3, 1],
            [3, 4, 5, 8],
        ];
        expect(findOptimalWild(riskMatrix)).toBe(3);
    });
});


