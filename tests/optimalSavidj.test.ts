import {findOptimalSavidj} from "../src/app";


describe('Test functon "findOptimalSavidj"', () => {
    it('When risk matrix is empty', () => {
        const riskMatrix = [
            [],
            [],
            []
        ]
        expect(findOptimalSavidj(riskMatrix)).toBe(1);
    });

    it('When risk matrix is 3x4 size', () => {
        const riskMatrix = [
            [1, 12, 4, 7],
            [6, 4, 9, 3],
            [9, 3, 1, 2],
        ];
        expect(findOptimalSavidj(riskMatrix)).toBe(2);
    });
});

