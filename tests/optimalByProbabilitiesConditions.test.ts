import {findOptimalByProbabilitiesConditions} from "../src/app";

describe('Test function "findOptimalByProbabilitiesConditions"', () => {
    it('When risk matrix is empty', () => {
        const riskMatrix = [
            [],
            [],
            []
        ];
        const probabilisticEvents = [0.4, 0.2, 0.3, 0.1];
        expect(findOptimalByProbabilitiesConditions(riskMatrix, probabilisticEvents)).toBe(1);
    });

    it('When risk matrix is 3x4 size', () => {
        const riskMatrix = [
            [1, 12, 4, 7],
            [6, 4, 9, 3],
            [9, 3, 1, 2],
        ];
        const probabilisticEvents = [0.25, 0.25, 0.25, 0.25];
        expect(findOptimalByProbabilitiesConditions(riskMatrix, probabilisticEvents)).toBe(3);
    });
});
