import {getRiskMatrix} from "../src/app";


const testRiskMatrixOne = {
    paymentMatrix: [
        [1, 12, 4, 7],
        [6, 4, 9, 3],
        [9, 3, 1, 2],
    ],
    riskMatrix: [
        [8, 0, 5, 0],
        [3, 8, 0, 4],
        [0, 9, 8, 5],
    ]
};

const testRiskMatrixTwo = {
    paymentMatrix: [
        [-1, 2, -3, 4, 5],
        [2, -2, 3, -4, 5],
        [-3, 3, -3, 4, -5],
        [4, -4, 4, -4, 5],
        [-5, 5, -5, 5, -5],
    ],
    riskMatrix: [
        [5, 3, 7, 1, 0],
        [2, 7, 1, 9, 0],
        [7, 2, 7, 1, 10],
        [0, 9, 0, 9, 0],
        [9, 0, 9, 0, 10],
    ]
};

const testRiskMatrixThird = {
    paymentMatrix: [
        []
    ],
    riskMatrix: [
       []
    ]
}

describe('Function "getRiskMatrix"', () => {

   it('When payment matrix is empty', () => {
       expect(getRiskMatrix(testRiskMatrixThird.paymentMatrix)).toEqual(testRiskMatrixThird.riskMatrix);
   });

   it('When payment matrix 3x4', () => {
       expect(getRiskMatrix(testRiskMatrixOne.paymentMatrix)).toEqual(testRiskMatrixOne.riskMatrix);
   });

   it('When payment matrix with negative elements', () => {
       expect(getRiskMatrix(testRiskMatrixTwo.paymentMatrix)).toEqual(testRiskMatrixTwo.riskMatrix);
   });
});

