import {getTransposeMatrix} from "../src/app";

describe("Function 'getTransposeMatrix'", () => {
   it('When matrix is empty', () => {
       expect(getTransposeMatrix([[]])).toEqual([[]]);
   });

   it('When matrix is square', () => {
       expect(getTransposeMatrix([[1, 2], [1, 2]])).toEqual([[1, 1], [2, 2]]);
   });

   it('When transpose matrix is not square', () => {
       const test = {
           data: [
               [1, 4, 3],
               [8, 2, 6],
               [7, 8, 3],
               [4, 9, 6],
               [7, 8, 1]
           ],
           result: [
               [1, 8, 7, 4, 7],
               [4, 2, 8, 9, 8],
               [3, 6, 3, 6, 1]
           ]
       }
       expect(getTransposeMatrix(test.data)).toEqual(test.result);
   });
});

