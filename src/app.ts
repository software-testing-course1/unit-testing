const tests = [
    {
        name: 'Тест 1',
        paymentMatrix: [
            [1, 12, 4, 7],
            [6, 4, 9, 3],
            [9, 3, 1, 2],
        ],
        probabilisticEvents: [0.25, 0.25, 0.25, 0.25],
        hurwiczCoeff: 1
    },
    {
        name: 'Тест 2',
        paymentMatrix: [
            [4, 3, 2, 3],
            [4, 2, 3, 1],
            [3, 4, 5, 8],
        ],
        probabilisticEvents: [0.4, 0.2, 0.3, 0.1],
        hurwiczCoeff: 0.3
    }
]
// tests.forEach(({name, paymentMatrix, probabilisticEvents,
//                    hurwiczCoeff, }) => {
//     console.log(name);
//     // Получаеи матрицу риска
//     const riskMatrix = getRiskMatrix(paymentMatrix);
//     const optimalByProbabilitiesConditions =
//         findOptimalByProbabilitiesConditions(riskMatrix,
//             probabilisticEvents);
//     const optimalWild = findOptimalWild(paymentMatrix);
//     const optimalSavidj = findOptimalSavidj(riskMatrix);
//     const optimalHurwitz = findOptimalHurwitz(paymentMatrix,
//         hurwiczCoeff);
//     console.log("Платежная матрица");
//     console.table(paymentMatrix);
//     console.log("Матрица риска");
//     console.table(riskMatrix);
//     console.log(`Коэффициент Гурвица ${hurwiczCoeff}`);
//     console.log(`Вероятностные условия
// ${probabilisticEvents}`);
//     console.log(`Номер стратегии для критерия, основанного на
// известных вероятностях условий:
// ${optimalByProbabilitiesConditions}`);
//     console.log(`Номер стратегии для критерия Вальда:
// ${optimalWild}`);
//     console.log(`Номер стратегии для критерия Сэвиджа:
// ${optimalSavidj}`);
//     console.log(`Номер стратегии для критерия Гурвица:
// ${optimalHurwitz}`);
// });
// Функция для получения транспонированной матрицы
export function getTransposeMatrix(m: number[][]): number[][] {
    return m[0].map((col, i) => m.map(row => row[i]));
}
// Функция для получения суммы элементов массива
export function sum(arr: number[]): number {
    return arr.reduce((previousValue, currentValue) =>
        previousValue + currentValue);
}
// Функция для получения матрицы риска
export function getRiskMatrix(paymentMatrix: number[][]): number[][]
{
    const transposeRiskMatrix =
        getTransposeMatrix(paymentMatrix).map((col: number[]) => {
            // Находим максимальный элемент в каждом столбце
            const maxElemInCol = Math.max(...col);
// Вычитаем из максимального элемента столбца каждый элемент столбца
            return col.map((num: number) => maxElemInCol - num);
        });
    return getTransposeMatrix(transposeRiskMatrix);
}
// Функция для нахождения оптимальной стратегии по критерию Вальда
export function findOptimalWild(paymentMatrix: number[][]): number {
    // Находим минимальный элемент в каждой строке
    const mins: number[] = paymentMatrix.map((row: number[]) => Math.min(...row));
    // Находим максимальный элемент среди минимальных
    const maxMins: number = Math.max(...mins);
    // Получаем номер максимальньго элемента
    return mins.indexOf(maxMins) + 1;
}
// Функция для нахождения оптимальной стратегии по критерию Сэвиджа
export function findOptimalSavidj(riskMatrix: number[][]): number {
    // Находим максимальный элемент в каждой строке
    const maxes: number[] = riskMatrix.map((row: number[]) =>
        Math.max(...row));
    // Находим минимальный элемент среди максимальных
    const minMaxes: number = Math.min(...maxes);
    // Получаем номер минимального элемента
    return maxes.indexOf(minMaxes) + 1;
}
// Функция для нахождения оптимальной стратегии по критерию Гурвица
export function findOptimalHurwitz(paymentMatrix: number[][],
                            hurwiczCoeff: number): number {
    const payments = paymentMatrix.map((row: number[]) => {
        const maxRowElem = Math.max(...row);
        const minRowElem = Math.min(...row);
        return (minRowElem * hurwiczCoeff) + ((1 -
            hurwiczCoeff) * maxRowElem);
    });
    const maxPayment = Math.max(...payments);
    return payments.indexOf(maxPayment) + 1;
}
// Функция для нахождения оптимальной стратегии по критерию основанном,
// на известных вероятностях условий
export function findOptimalByProbabilitiesConditions(riskMatrix:
                                                  number[][], probabilisticEvents: number[]): number {
    const averageRisks: number[] = riskMatrix.map((row) => {
        return sum(row.map((num, j) => {
            return probabilisticEvents[j] * num;
        }));
    });
    const minAverageRisk = Math.min(...averageRisks);
    const minAverageRiskIndex =
        averageRisks.indexOf(minAverageRisk);
    return minAverageRiskIndex + 1;
}

// console.log(getRiskMatrix([[1, 2], [1, 2]]));
