# Software testing course
## Lab 1: Unit testing

To run tests, you need to enter several commands in the console

1. `npm install`
2. `npm run test`

The test results are in the file /results/jest-stare/index.html
